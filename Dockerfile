FROM debian:stable as source
ARG V8_VERSION=7.5-lkgr
# These versions can be found by looking in DEPS of the current V8 branch
ARG DEPOT_TOOLS_VERSION=7e7523be4e21b0841ae815ef37521a5476f68549
ARG GN_VERSION=64b846c96daeb3eaf08e26d8a84d8451c6cb712b

WORKDIR /tmp

RUN apt-get update \
    && apt-get install -y \
        git \
        curl \
        python \
    # Clone tools for Chromium development (V8 is in Chromium).
    && git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git --progress --verbose \
    && cd /tmp/depot_tools \
    && git checkout ${DEPOT_TOOLS_VERSION} \
    && cd - \
    && PATH=$PATH:/tmp/depot_tools \
    # Fetch V8 source
    && fetch v8 \
    && cd ./v8 \
    && git checkout ${V8_VERSION} \
    && gclient sync -D --force --reset \
    # Cleanup
    && apt-get remove --purge -y \
        git \
        curl \
        python \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

FROM alpine

COPY --from=source /tmp/v8 /tmp/v8

RUN apk add --virtual .v8-build-dependencies \
        $PHPIZE_DEPS \
        curl \
        g++ \
        gcc \
        glib-dev \
        icu-dev \
        libstdc++ \
        linux-headers \
        make \
        ninja \
        python \
        tar \
        xz \
    && apk add --virtual .gn-build-dependencies \
        binutils-gold \
        clang \
        git \
        llvm \
    # Compile gn (GN is a meta-build system that generates build files for Ninja)
    && cp -f /usr/bin/ld.gold /usr/bin/ld \
    # Clone and build gn
    && git clone --single-branch https://gn.googlesource.com/gn /tmp/gn --progress --verbose \
    && cd /tmp/gn \
    && git checkout ${GN_VERSION} \
    && PATH=$PATH:/usr/lib/llvm/bin python build/gen.py \
    && PATH=$PATH:/usr/lib/llvm/bin ninja -j"$(nproc)" -C out \
    # Replace prebuilt gn with one that works on Alpine
    && cp -f /tmp/gn/out/gn /tmp/v8/buildtools/linux64/gn \
    # Generate a ninja build build script
    && cd /tmp/v8 \
    && tools/dev/v8gen.py -vv x64.release -- \
        binutils_path=\"/usr/bin\" \
        target_os=\"linux\" \
        target_cpu=\"x64\" \
        v8_target_cpu=\"x64\" \
        v8_use_external_startup_data=false \
        v8_enable_future=true \
        is_official_build=true \
        is_component_build=true \
        is_cfi=false \
        is_clang=false \
        use_custom_libcxx=false \
        use_sysroot=false \
        use_gold=false \
        treat_warnings_as_errors=false \
        symbol_level=0 \
    # Use ninja (think make, but Googley-er) to build V8
    && ninja -j"$(nproc)" -C out.gn/x64.release/ \
    && mkdir -p /opt/v8/lib \
    && cp -f out.gn/x64.release/lib*.so /opt/v8/lib/\
    && cp -f out.gn/x64.release/*_blob.bin /opt/v8/lib/ || true \
    && cp -f out.gn/x64.release/icudtl.dat /opt/v8/lib/ || true \
    && cp -r include /opt/v8/include \
    && rm -rf /tmp/v8
